# Power set generator
# Generates all possible permutations of all possible substrings of a charset

# Imports
from itertools import product
import os

# All substrings
substrings = []

# All permutations of all substrings
generated = []

# Maximum character set length
MAX_LENGTH = 5

# First iteration flag
first_time = True

# File mode flag
file_mode = False

# File path
file_path = ""

# File handle
f = None

# Error message
error = "Unknown error"


# ------------------------------------------------------------------------------
# GENERATORS
# ------------------------------------------------------------------------------

# Substrings
def gen_substrings(s, output):
    n = len(s)
    # Binary mask, 2^n elements for n length
    # Take only characters with binary 1 in truth table rows
    masks = [1 << j for j in range(n)]
    for i in range(2 ** n):
        ss = ''.join([str(s[j]) for j in range(n) if (masks[j] & i)])
        substrings.append(ss)

    # Remove empty string
    substrings.pop(0)

    if output:
        pretty_print(substrings)

    return substrings


# Cartesian products
def gen_cartesian_products(s, length):
    return [''.join(i) for i in product(s, repeat=length)]


# Cartesian products of substrings
def generate_all(charset, output):
    gen_substrings(charset, False)
    for ss in substrings:
        cps = gen_cartesian_products(ss, len(ss))
        for cp in cps:
            # O(1) lookup time complexity
            if cp not in generated:
                generated.append(cp)
    if output:
        pretty_print(generated)
    else:
        print(len(generated), "sequences generated (check file for output)")


# ------------------------------------------------------------------------------

def reset_arrays():
    del substrings[:]
    del generated[:]


def pretty_print(sequence):
    sequence.sort()
    print()
    print(len(sequence), "elements generated: ")
    print(', '.join(sequence))
    print()


def format_charset(charset):
    return ''.join(sorted(list(set(charset.lower()))))


def is_valid(charset):
    global error
    if len(charset) > MAX_LENGTH:
        error = "Maximum input length exceeded. Max length = " + str(MAX_LENGTH)
        return False
    else:
        return True


def hello():
    print()
    print("-------------------------------------------------------------------------------")
    print("-------------------------- POWER-SET GENERATOR LOADED -------------------------")
    print("-------------------------------------------------------------------------------")
    print()


def bye():
    print()
    print("-------------------------------------------------------------------------------")
    print("-------------------------- POWER-SET GENERATOR CLOSED -------------------------")
    print("-------------------------------------------------------------------------------")
    print()


def help_message():
    print()
    print("- Power-Set Generator | Help (type anything to return to the previous screen) -")
    print()
    print("This tool generates the power-set of a string and computes all possible unique")
    print("combinations of every element in that set. The time complexity of this program")
    print("makes it useless for input lengths larger than 6 on most computers. The current")
    print("maximum is", MAX_LENGTH, ". Commands available: ")
    print()
    print("\th\t Displays this help page")
    print("\tf\t Toggle file mode (write to file instead of console)")
    print("\tq\t Quit")
    print()
    _ = input("")


def print_charsets(charset, input_charset):
    charset_length = len(charset)
    print("Chosen character set: ", input_charset)
    print("Processing...")
    print("Unique characters found: ", charset_length)
    print("Final character set: ", charset)


def search():
    search_choice = input("Would you like to search this array? (y/n): ")
    search_choice = search_choice.lower()
    if search_choice == "y" or search_choice == "yes":
        while True:
            pattern = input("Search for an element (e to exit): ")
            pattern = pattern.lower()
            if pattern == "e" or pattern == "exit":
                break
            else:
                if pattern in generated:
                    print("Sequence \'", pattern, "\' FOUND at index ", generated.index(pattern))
                else:
                    print("Sequence \'", pattern, "\' NOT FOUND")


def write_to_file(path):
    abs_path = os.path.abspath(path)
    file = open(abs_path, 'r+')
    file.truncate()
    for e in generated:
        file.write(e + '\n')
    file.close()


def main():
    global first_time, file_mode, file_path, error
    hello()
    while True:
        print("-------------------------------------------------------------------------------")
        if first_time:
            input_charset = input("Type your desired character set to begin (\'h\' for help): ")
            first_time = False
        else:
            input_charset = input("Character set: ")
        print("-------------------------------------------------------------------------------")

        # Help
        if input_charset == "h":
            help_message()
            continue
        # File mode toggle
        elif input_charset == "f":
            file_mode = not file_mode
            if file_mode:
                print("File mode is now ON")
            else:
                print("File mode is now OFF")
            continue
        # Quit
        elif input_charset == "q":
            break
        # Proceed
        else:
            pass

        print()
        print("Validating...")
        charset = format(input_charset)
        if is_valid(charset):
            print("PASS - Proceeding")
            print()
            print_charsets(charset, input_charset)
            reset_arrays()
            print("Working...")
            if file_mode:
                print()
                file_path = input("Relative path to output file: ")
                generate_all(charset, False)
                write_to_file(file_path)
            else:
                generate_all(charset, True)
                search()
        else:
            print("FAIL - Character set invalid for the following reason: ")
            print(error)
        print("Starting over...")
        print()
    bye()


main()
