# (Inefficient) Power-set Generator
This Python script is one of my early attempts (~2014) at generating the power-set of a given string. It computes all possible permutations of the string's set of characters.

The time complexity of this code (exponential) makes it useless for input lengths larger than 6 on most computers.
